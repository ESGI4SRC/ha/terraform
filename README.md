# terraform

# terraform plan

prevision de ce qui va etre appliquer

# terraform apply

application des modifications

# terraform init

initialise le provider

# terraform destroy

Detruit toutes les ressources

# Syntaxe tf

Prévenir la destruction pour eviter de le détruire automatiquement avec terraform destroy

```tf
lifecycle {
        prevent_destroy = true
    }
```

# Graph inventory

ansible-inventory --graph

# Téléchargement des roles via ansible-galaxy

ansible-galaxy collection install -r collections/requirements.yml

# Lancement du playbook

ansible-playbook playbook.yml

# Lancement du playbook pour le tag locales

ansible-playbook playbook.yml -t locales
