# Affichage du FQDN public

output "sulabs_terraform_src_public_fqdn" {
    value = azurerm_public_ip.sulabs_src_terraform.fqdn
}

# Affichage de l'ip public

output "sulabs_terraform_lb_public_ip" {
    value = azurerm_public_ip.sulabs_src_terraform.ip_address
}

# Affichage de l'ip public de la Jumpbox

output "sulabs_terraform_jumpbox_public_ip" {
    value = azurerm_public_ip.sulabs_src_terraform_jumpbox_public_ip.ip_address
}
