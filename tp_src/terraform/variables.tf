# Localisation par défaut de nos ressources

variable "location" {
    description = "Location where resources will be created"
    default = "France Central"
}

# Nom du groupe de ressources

variable "resource_group_name" {
    description = "Name of the resource group"
    default = "terraform-src"
}

# Port de l'application

variable "application_port" {
    description = "Port exposed to the external loadbalancer"
    default = 80
}

# Databases Name (DNS de la BDD) doit etre unique dans le WWW

variable "mariadb_server_name" {
    description = "Database server name"
    default = "terraform-src-sulabs-mariadb-server-dylan"
}

# Databases Username

variable "mariadb_server_username" {
    type = string
    description = "Database admin server username"
    default = "sciencesu"
}

# IP Public Perso

variable "work_ip_address" {
    type = string
    description = "Office IP address"
    default = "XX.XX.XX.XX"
}

# Tags

variable "tags" {
    description = "Map of tags"
    type        = map(string)

    default = {
        environment = "labs"
    }
}

